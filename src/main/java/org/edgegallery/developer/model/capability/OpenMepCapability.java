/*
 *    Copyright 2020-2021 Huawei Technologies Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.edgegallery.developer.model.capability;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class OpenMepCapability {

    private String detailId;

    private String groupId;

    private String service;

    private String serviceEn;

    private String version;

    private String description;

    private String descriptionEn;

    private String provider;

    // download or show api
    private String apiFileId;

    private String guideFileId;

    private String guideFileIdEn;

    private String uploadTime;

    private int port;

    private String host;

    private String protocol;

    private String appId;

    private String packageId;

    private String userId;

    public OpenMepCapability() {
    }

    /**
     * OpenMepCapabilityDetail.
     */
    public OpenMepCapability(String id, String groupId, String service, String serviceEn, String version,
        String description) {
        this.detailId = id;
        this.groupId = groupId;
        this.service = service;
        this.serviceEn = serviceEn;
        this.version = version;
        this.description = description;
    }

    /**
     * setGuideFileIdEn.
     */
    public void setGuideFileIdEn(String guideFileIdEn) {
        if (StringUtils.isBlank(guideFileIdEn)) {
            this.guideFileIdEn = this.guideFileId;
        } else {
            this.guideFileIdEn = guideFileIdEn;
        }
    }

    /**
     * setServiceEn.
     */
    public void setServiceEn(String serviceEn) {
        if (StringUtils.isBlank(serviceEn)) {
            this.serviceEn = this.service;
        } else {
            this.serviceEn = serviceEn;
        }
    }

    /**
     * setDescriptionEn.
     */
    public void setDescriptionEn(String descriptionEn) {
        if (StringUtils.isBlank(descriptionEn)) {
            this.descriptionEn = this.description;
        } else {
            this.descriptionEn = descriptionEn;
        }
    }

}
