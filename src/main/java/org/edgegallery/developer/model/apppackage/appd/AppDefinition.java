/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.model.apppackage.appd;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppDefinition {
    @Valid
    @NotBlank
    @JsonProperty("tosca_definitions_version")
    private String toscaDefinitionsVersion = "tosca_simple_profile_1_2";

    @JsonProperty("description")
    private String description = "EG MEC App Description";

    @Valid
    @NotEmpty
    @JsonProperty("imports")
    private List<String> imports = Arrays.asList("nfv_vnfd_types_v1_0_20190924.yaml");

    @Valid
    @NotNull
    @JsonProperty("metadata")
    private Metadata metadata = new Metadata();

    @Valid
    @NotBlank
    @JsonProperty("topology_template")
    private TopologyTemplate topologyTemplate;

}
