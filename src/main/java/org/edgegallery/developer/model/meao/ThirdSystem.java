package org.edgegallery.developer.model.meao;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ThirdSystem {
    private String id;

    private String systemName;

    private String systemType;

    private String userId;

    private String version;

    private String url;

    private String ip;

    private int port;

    private String region;

    private String username;

    private String password;

    private String product;

    private String vendor;

    private String tokenType;

    private String status;

    private String icon;

    private String configContent;
}
