package org.edgegallery.developer.model.meao;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateImageEntity {

    private String appId;

    private String fsIp;

    private String imageName;

    private String imageFormat = "qcow2";

}
