/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.model.resource.pkgspec;

public class PkgSpecConstants {

    private PkgSpecConstants() {
        throw new IllegalStateException("PkgSpecConstants class");
    }

    public static final String FLAVOR_SPEC_CPU = "cpu";

    public static final String FLAVOR_SPEC_MEM = "mem";

    public static final String FLAVOR_SPEC_DISK = "disk";

    public static final String FLAVOR_SPEC_GPU = "gpu";

    public static final String FLAVOR_SPEC_HOST = "host_aggr";

    public static final String PKG_SPEC_SUPPORT_DYNAMIC_FLAVOR = "PKG_SPEC_SUPPORT_DYNAMIC_FLAVOR";

    public static final String PKG_SPEC_SUPPORT_FIXED_FLAVOR = "PKG_SPEC_SUPPORT_FIXED_FLAVOR";

    public static final String PKG_SPEC_FIRST_LOCAL_DISK = "PKG_SPEC_FIRST_LOCAL_DISK";

    public static final String PKG_SPEC_SECOND_LOCAL_DISK = "PKG_SPEC_SECOND_LOCAL_DISK";

    public static final String PKG_SPEC_FIRST_CLOUD_DISK = "PKG_SPEC_FIRST_CLOUD_DISK";

    public static final String PKG_SPEC_SECOND_CLOUD_DISK = "PKG_SPEC_SECOND_CLOUD_DISK";

    public static final String PKG_SPEC_CLOUD_MANAGEMENT = "PKG_SPEC_CLOUD_MANAGEMENT";

}
