/*
 *    Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.edgegallery.developer.exception;

import java.io.FileNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import org.apache.servicecomb.swagger.invocation.exception.InvocationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingMatrixVariableException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionConvert {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionConvert.class);

    private static final String HTTP_REQUEST_ERR_MSG = "http request exception";

    /**
     * Handle Exception.
     *
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public RestReturn defaultException(HttpServletRequest request, Exception e) {
        if (e instanceof MissingMatrixVariableException || e instanceof HttpMessageNotReadableException
            || e instanceof MethodArgumentNotValidException || e instanceof MissingPathVariableException) {
            LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
            return badRequestResponse(request, e);
        }
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
            .error(Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase()).message(e.getMessage())
            .path(request.getRequestURI()).build();
    }

    private RestReturn badRequestResponse(HttpServletRequest request, Exception e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.BAD_REQUEST.getStatusCode())
            .error(Response.Status.BAD_REQUEST.getReasonPhrase()).message(e.getMessage()).path(request.getRequestURI())
            .build();
    }

    /**
     * Handler IllegalArgumentException.
     *
     * @return
     */
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public RestReturn illegalArgumentException(HttpServletRequest request, IllegalArgumentException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return badRequestResponse(request, e);
    }

    /**
     * Handle EntityNotFoundException.
     *
     * @return
     */
    @ExceptionHandler(value = EntityNotFoundException.class)
    @ResponseBody
    public RestReturn entityNotFoundException(HttpServletRequest request, EntityNotFoundException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.NOT_FOUND.getStatusCode())
            .error(Response.Status.NOT_FOUND.getReasonPhrase()).message(e.getMessage()).path(request.getRequestURI())
            .retCode(e.getErrMsg().getRetCode()).params(e.getErrMsg().getParams()).build();
    }

    /**
     * Handle FileNotFoundException.
     *
     * @return
     */
    @ExceptionHandler(value = FileNotFoundException.class)
    @ResponseBody
    public RestReturn fileNotFoundException(HttpServletRequest request, FileNotFoundException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.NOT_FOUND.getStatusCode())
            .error(Response.Status.NOT_FOUND.getReasonPhrase()).message(e.getMessage()).path(request.getRequestURI())
            .build();

    }

    /**
     * Handle FileNotFoundException.
     *
     * @return
     */
    @ExceptionHandler(value = FileFoundFailException.class)
    @ResponseBody
    public RestReturn fileFoundFailException(HttpServletRequest request, FileFoundFailException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.NOT_FOUND.getStatusCode())
            .error(Response.Status.NOT_FOUND.getReasonPhrase()).message(e.getMessage()).path(request.getRequestURI())
            .retCode(e.getErrMsg().getRetCode()).params(e.getErrMsg().getParams()).build();

    }

    /**
     * Handle InvocationException.
     *
     * @return
     */
    @ExceptionHandler(value = InvocationException.class)
    @ResponseBody
    public RestReturn invocationException(HttpServletRequest request, InvocationException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(e.getStatusCode()).error(e.getReasonPhrase())
            .message(e.getErrorData().toString()).path(request.getRequestURI()).build();
    }

    /**
     * Handle IllegalRequestException.
     */
    @ExceptionHandler(value = IllegalRequestException.class)
    @ResponseBody
    public RestReturn illegalRequestException(HttpServletRequest request, IllegalRequestException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.BAD_REQUEST.getStatusCode())
            .error(Response.Status.BAD_REQUEST.getReasonPhrase()).message(e.getMessage()).path(request.getRequestURI())
            .retCode(e.getErrMsg().getRetCode()).params(e.getErrMsg().getParams()).build();
    }

    /**
     * Handle InternalException.
     */
    @ExceptionHandler(value = DataBaseException.class)
    @ResponseBody
    public RestReturn dataBaseException(HttpServletRequest request, DataBaseException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
            .error(Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase()).message(e.getMessage())
            .path(request.getRequestURI()).retCode(e.getErrMsg().getRetCode()).params(e.getErrMsg().getParams())
            .build();
    }

    /**
     * Handle FileOperateException.
     */
    @ExceptionHandler(value = FileOperateException.class)
    @ResponseBody
    public RestReturn fileOperateException(HttpServletRequest request, FileOperateException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
            .error(Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase()).message(e.getMessage())
            .path(request.getRequestURI()).retCode(e.getErrMsg().getRetCode()).params(e.getErrMsg().getParams())
            .build();
    }

    /**
     * Handle HarborException.
     */
    @ExceptionHandler(value = HarborException.class)
    @ResponseBody
    public RestReturn fileOperateException(HttpServletRequest request, HarborException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
            .error(Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase()).message(e.getMessage())
            .path(request.getRequestURI()).retCode(e.getErrMsg().getRetCode()).params(e.getErrMsg().getParams())
            .build();
    }

    /**
     * Handle UnauthorizedException.
     */
    @ExceptionHandler(value = UnauthorizedException.class)
    @ResponseBody
    public RestReturn unauthorizedException(HttpServletRequest request, UnauthorizedException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.UNAUTHORIZED.getStatusCode())
            .error(Response.Status.UNAUTHORIZED.getReasonPhrase()).message(e.getMessage())
            .path(request.getRequestURI()).retCode(e.getErrMsg().getRetCode()).params(e.getErrMsg().getParams())
            .build();
    }

    /**
     * Handle ForbiddenException.
     */
    @ExceptionHandler(value = ForbiddenException.class)
    @ResponseBody
    public RestReturn unauthorizedException(HttpServletRequest request, ForbiddenException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.FORBIDDEN.getStatusCode())
            .error(Response.Status.FORBIDDEN.getReasonPhrase()).message(e.getMessage())
            .path(request.getRequestURI()).retCode(e.getErrMsg().getRetCode()).params(e.getErrMsg().getParams())
            .build();
    }

    /**
     * Handle UnknownException.
     */
    @ExceptionHandler(value = DeveloperException.class)
    @ResponseBody
    public RestReturn unauthorizedException(HttpServletRequest request, DeveloperException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
            .error(Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase()).message(e.getMessage())
            .path(request.getRequestURI()).retCode(e.getErrMsg().getRetCode()).params(e.getErrMsg().getParams())
            .build();
    }

    /**
     * Handle RestfulRequestException.
     */
    @ExceptionHandler(value = RestfulRequestException.class)
    @ResponseBody
    public RestReturn restfulRequestException(HttpServletRequest request, RestfulRequestException e) {
        LOGGER.error(HTTP_REQUEST_ERR_MSG, e);
        return RestReturn.builder().code(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
            .error(Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase()).message(e.getMessage())
            .path(request.getRequestURI()).retCode(e.getErrMsg().getRetCode()).params(e.getErrMsg().getParams())
            .build();
    }


}


