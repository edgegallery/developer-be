/*
 *    Copyright 2021 Huawei Technologies Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.edgegallery.developer.controller.application.container;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Pattern;
import org.apache.servicecomb.provider.rest.common.RestSchema;
import org.edgegallery.developer.exception.DeveloperException;
import org.edgegallery.developer.filter.security.AccessUserUtil;
import org.edgegallery.developer.model.common.User;
import org.edgegallery.developer.model.restful.ErrorRespDto;
import org.edgegallery.developer.model.restful.OperationInfoRep;
import org.edgegallery.developer.model.reverseproxy.SshResponseInfo;
import org.edgegallery.developer.service.application.container.ContainerAppOperationService;
import org.edgegallery.developer.service.proxy.ReverseProxyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RestSchema(schemaId = "containerAppOperation")
@RequestMapping("/mec/developer/v2/applications")
@Api(tags = "containerAppOperation")
@Validated
public class ContainerAppOperationCtl {
    private static final String REGEX_UUID = "[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}";

    @Autowired
    private ContainerAppOperationService containerAppOperationService;

    @Autowired
    private ReverseProxyService reverseProxyService;

    /**
     * instantiate a container app.
     */
    @ApiOperation(value = "instantiate a container app.", response = Boolean.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = Boolean.class),
        @ApiResponse(code = 400, message = "Bad Request", response = ErrorRespDto.class)
    })
    @RequestMapping(value = "/{applicationId}/containers/action/launch", method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('DEVELOPER_TENANT') || hasRole('DEVELOPER_ADMIN')")
    public ResponseEntity<OperationInfoRep> instantiateContainerApp(
        @Pattern(regexp = REGEX_UUID, message = "applicationId must be in UUID format")
        @ApiParam(value = "applicationId", required = true) @PathVariable("applicationId") String applicationId) {
        User user = AccessUserUtil.getUser();
        OperationInfoRep result = containerAppOperationService.instantiateContainerApp(applicationId, user);
        return ResponseEntity.ok(result);
    }

    /**
     * get ssh info.
     */
    @ApiOperation(value = "get ssh url", response = SshResponseInfo.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = SshResponseInfo.class),
        @ApiResponse(code = 400, message = "Bad Request", response = ErrorRespDto.class)
    })
    @RequestMapping(value = "/{applicationId}/containers/action/ssh", method = RequestMethod.GET)
    @PreAuthorize("hasRole('DEVELOPER_TENANT') || hasRole('DEVELOPER_ADMIN')")
    public ResponseEntity getContainerSshInfo(
        @ApiParam(value = "applicationId", required = true) @PathVariable("applicationId") String applicationId,
        HttpServletRequest request) {

        String[] cookies = request.getHeader("Cookie").split(";");
        String xsrfValue = "";
        for (String cookie : cookies) {
            if (cookie.contains("XSRF-TOKEN")) {
                xsrfValue = cookie.split("=")[1];
            }
        }
        if ("".equals(xsrfValue)) {
            throw new DeveloperException("failed to get XSRF-TOKEN by cookie");
        }
        SshResponseInfo sshResponseInfo = reverseProxyService
            .getContainerSshResponseInfo(applicationId, AccessUserUtil.getUserId(), xsrfValue);
        return ResponseEntity.ok(sshResponseInfo);
    }

}
