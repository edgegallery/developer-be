/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.service.apppackage.csar.creater;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.edgegallery.developer.model.application.vm.VMApplication;
import org.edgegallery.developer.model.application.vm.VirtualMachine;
import org.edgegallery.developer.model.apppackage.CloudMgtImageDesc;
import org.edgegallery.developer.model.resource.vm.VMImage;
import org.edgegallery.developer.service.recource.vm.VMImageService;
import org.edgegallery.developer.util.SpringContextUtil;

public class VMCloudMgtPkgCreator extends PackageFileCreator {

    private VMImageService vmImageService = (VMImageService) SpringContextUtil.getBean(VMImageService.class);

    private VMApplication application;

    private static final String APPD_IMAGE_DES_PATH = "/Image/SwImageDesc.json";

    /**
     * constructor.
     *
     * @param application application
     * @param packageId packageId
     */
    public VMCloudMgtPkgCreator(VMApplication application, String packageId) {
        super(application, packageId);
        this.application = application;

    }

    /**
     * generate application package.
     */
    public String generateVmPackageFile() {
        String packagePath = getPackagePath();
        if (!copyPackageTemplateFile()) {
            LOGGER.error("copy package template file fail, package dir:{}", packagePath);
            return null;
        }
        configImageMfFile();
        generateImageDesFile();
        String compressPath = compressCloudMgtFile();
        if (compressPath == null) {
            LOGGER.error("package compress fail");
            return null;
        }
        return compressPath;
    }

    /**
     * generate image description  file.
     */
    public void generateImageDesFile() {
        List<CloudMgtImageDesc> imageDescs = new ArrayList<>();
        for (VirtualMachine vm : application.getVmList()) {
            int imageId = vm.getImageId();
            VMImage vmImage = vmImageService.getVmImageById(imageId);
            CloudMgtImageDesc imageDesc = new CloudMgtImageDesc(vmImage);
            imageDescs.add(imageDesc);
        }
        // write data into imageJson file
        File imageJson = new File(getPackagePath() + APPD_IMAGE_DES_PATH);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonElement jsonElement = JsonParser.parseString(gson.toJson(imageDescs));
        writeFile(imageJson, gson.toJson(jsonElement));
    }

}
