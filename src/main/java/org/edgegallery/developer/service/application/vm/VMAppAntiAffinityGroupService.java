/*
 *    Copyright 2022 Huawei Technologies Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.edgegallery.developer.service.application.vm;

import java.util.List;
import org.edgegallery.developer.model.application.vm.AntiAffinityGroup;

public interface VMAppAntiAffinityGroupService {

    /**
     * create antiaffinitygroup.
     *
     * @param applicationId vm application id
     * @param antiaffinitygroup antiaffinitygroup
     * @return
     */
    AntiAffinityGroup createAntiAffinityGroup(String applicationId, AntiAffinityGroup antiaffinitygroup);

    /**
     * get all antiaffinitygroup.
     *
     * @param applicationId vm application id
     * @param vmId vm Id
     * @return
     */
    List<AntiAffinityGroup> getAllAntiAffinityGroups(String applicationId, String vmId);

    /**
     * get antiaffinitygroup.
     *
     * @param applicationId vm application id
     * @param antiaffinitygroupId antiaffinitygroup id
     * @return
     */
    AntiAffinityGroup getAntiAffinityGroup(String applicationId, String antiaffinitygroupId);

    /**
     * modify antiaffinitygroup.
     *
     * @param applicationId vm application id
     * @param antiaffinitygroupId antiaffinitygroup id
     * @param antiaffinitygroup needed update antiaffinitygroup
     * @return
     */
    AntiAffinityGroup modifyAntiAffinityGroup(String applicationId, String antiaffinitygroupId,
        AntiAffinityGroup antiaffinitygroup);

    /**
     * delete antiaffinitygroup with app id and antiaffinitygroup id.
     *
     * @param applicationId vm application id
     * @param antiaffinitygroupId antiaffinitygroup id
     * @return
     */
    Boolean deleteAntiAffinityGroup(String applicationId, String antiaffinitygroupId);

    /**
     * delete antiaffinitygroup with app id.
     *
     * @param applicationId vm application id
     * @return
     */
    Boolean deleteAntiAffinityGroupByAppId(String applicationId);
}
