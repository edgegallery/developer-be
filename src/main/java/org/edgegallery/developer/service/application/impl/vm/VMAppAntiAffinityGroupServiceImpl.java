/*
 *    Copyright 2022 Huawei Technologies Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.edgegallery.developer.service.application.impl.vm;

import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.edgegallery.developer.common.ResponseConsts;
import org.edgegallery.developer.exception.DataBaseException;
import org.edgegallery.developer.exception.DeveloperException;
import org.edgegallery.developer.exception.EntityNotFoundException;
import org.edgegallery.developer.mapper.application.vm.AntiAffinityGroupMapper;
import org.edgegallery.developer.model.application.vm.AntiAffinityGroup;
import org.edgegallery.developer.service.application.vm.VMAppAntiAffinityGroupService;
import org.edgegallery.developer.service.application.vm.VMAppVmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("vmAppAntiAffinityGroupService")
public class VMAppAntiAffinityGroupServiceImpl implements VMAppAntiAffinityGroupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VMAppAntiAffinityGroupServiceImpl.class);

    private static final int MAX_ANTI_AFFINITY_GROUP_NAME_LENGTH = 255;

    @Autowired
    private AntiAffinityGroupMapper antiAffinityGroupMapper;

    @Autowired
    private VMAppVmService vmAppVmService;

    @Override
    public AntiAffinityGroup createAntiAffinityGroup(String applicationId, AntiAffinityGroup antiAffinityGroup) {
        antiAffinityGroup.setId(UUID.randomUUID().toString());
        List<AntiAffinityGroup> existAntiAffinityGroups = getAllAntiAffinityGroups(applicationId, null);
        if (isNameExist(antiAffinityGroup.getId(), antiAffinityGroup, existAntiAffinityGroups)) {
            LOGGER.error("Name already exists and cannot be created.");
            throw new DeveloperException("Name already exists and cannot be created.",
                ResponseConsts.RET_CREATE_DATA_FAIL);
        }
        if (StringUtils.isEmpty(antiAffinityGroup.getName())
            || antiAffinityGroup.getName().length() > MAX_ANTI_AFFINITY_GROUP_NAME_LENGTH) {
            LOGGER.error("Anti affinity group name is illegal.");
            throw new DeveloperException("Anti affinity group name is illegal.", ResponseConsts.RET_CREATE_DATA_FAIL);
        }
        if (!isVmExist(applicationId, antiAffinityGroup)) {
            LOGGER.error("VM does not exist.");
            throw new DeveloperException("VM does not exist.", ResponseConsts.RET_CREATE_DATA_FAIL);
        }
        if (antiAffinityGroup.getVmIdList().isEmpty()) {
            LOGGER.error("VM list cannot be null.");
            throw new DeveloperException("VM list cannot be null.", ResponseConsts.RET_CREATE_DATA_FAIL);
        }
        int res = antiAffinityGroupMapper.createAntiAffinityGroup(applicationId, antiAffinityGroup);
        if (res < 1) {
            LOGGER.error("Create antiaffinitygroup in db error.");
            throw new DataBaseException("Create antiaffinitygroup in db error.", ResponseConsts.RET_CREATE_DATA_FAIL);
        }
        return antiAffinityGroup;
    }

    @Override
    public List<AntiAffinityGroup> getAllAntiAffinityGroups(String applicationId, String vmId) {
        List<AntiAffinityGroup> antiAffinityGroups = antiAffinityGroupMapper
            .getAllAntiAffinityGroupsByAppId(applicationId);
        if (!StringUtils.isEmpty(vmId) && !antiAffinityGroups.isEmpty()) {
            antiAffinityGroups.removeIf(antiAffinityGroup -> !antiAffinityGroup.getVmIdList().contains(vmId));
        }
        return antiAffinityGroups;
    }

    @Override
    public AntiAffinityGroup getAntiAffinityGroup(String applicationId, String antiAffinityGroupId) {
        AntiAffinityGroup antiAffinityGroup = antiAffinityGroupMapper
            .getAntiAffinityGroupById(applicationId, antiAffinityGroupId);
        if (antiAffinityGroup == null) {
            LOGGER.error("antiaffinitygroup is not exist.");
            throw new EntityNotFoundException("antiaffinitygroup is not exist.", ResponseConsts.RET_QUERY_DATA_EMPTY);
        }
        return antiAffinityGroup;
    }

    @Override
    public AntiAffinityGroup modifyAntiAffinityGroup(String applicationId, String antiAffinityGroupId,
        AntiAffinityGroup antiAffinityGroup) {
        List<AntiAffinityGroup> existAntiAffinityGroups = getAllAntiAffinityGroups(applicationId, null);
        if (isNameExist(antiAffinityGroupId, antiAffinityGroup, existAntiAffinityGroups)) {
            LOGGER.error("Name already exists and cannot be modified.");
            throw new DeveloperException("Name already exists and cannot be modified.",
                ResponseConsts.RET_UPDATE_DATA_FAIL);
        }
        if (!isVmExist(applicationId, antiAffinityGroup)) {
            LOGGER.error("VM does not exist.");
            throw new DeveloperException("VM does not exist.", ResponseConsts.RET_UPDATE_DATA_FAIL);
        }
        int res = antiAffinityGroupMapper
            .modifyAntiAffinityGroup(applicationId, antiAffinityGroupId, antiAffinityGroup);
        if (res < 1) {
            LOGGER.error("modify antiaffinitygroup in db error.");
            throw new DataBaseException("modify antiaffinitygroup in db error.", ResponseConsts.RET_UPDATE_DATA_FAIL);
        }
        return getAntiAffinityGroup(applicationId, antiAffinityGroupId);
    }

    @Override
    public Boolean deleteAntiAffinityGroup(String applicationId, String antiAffinityGroupId) {
        int res = antiAffinityGroupMapper.deleteAntiAffinityGroup(applicationId, antiAffinityGroupId);
        if (res < 1) {
            LOGGER.error("Delete antiaffinitygroup in db error.");
            throw new DataBaseException("Delete antiaffinitygroup in db error.", ResponseConsts.RET_DELETE_DATA_FAIL);
        }
        return true;
    }

    @Override
    public Boolean deleteAntiAffinityGroupByAppId(String applicationId) {
        List<AntiAffinityGroup> antiAffinityGroups = getAllAntiAffinityGroups(applicationId, null);
        for (AntiAffinityGroup antiAffinityGroup : antiAffinityGroups) {
            deleteAntiAffinityGroup(applicationId, antiAffinityGroup.getId());
        }
        return true;
    }

    private Boolean isNameExist(String antiAffinityGroupId, AntiAffinityGroup antiAffinityGroup,
        List<AntiAffinityGroup> existAntiAffinityGroups) {
        for (AntiAffinityGroup existAntiAffinityGroup : existAntiAffinityGroups) {
            if (!existAntiAffinityGroup.getId().equals(antiAffinityGroupId) && existAntiAffinityGroup.getName()
                .equals(antiAffinityGroup.getName())) {
                return true;
            }
        }
        return false;
    }

    private Boolean isVmExist(String applicationId, AntiAffinityGroup antiAffinityGroup) {
        List<String> vmIdList = antiAffinityGroup.getVmIdList();
        try {
            for (String vmId : vmIdList) {
                vmAppVmService.getVm(applicationId, vmId);
            }
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

}
