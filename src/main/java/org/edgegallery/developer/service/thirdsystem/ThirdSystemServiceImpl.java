package org.edgegallery.developer.service.thirdsystem;

import java.util.Collections;
import java.util.List;
import org.edgegallery.developer.model.meao.ThirdSystem;
import org.edgegallery.developer.util.HttpClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service("thirdSystemService")
public class ThirdSystemServiceImpl implements ThirdSystemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThirdSystemServiceImpl.class);

    @Override
    public List<ThirdSystem> getMeaoSystems() {
        List<ThirdSystem> thirdSystems = HttpClientUtil.getThirdSystems();
        if (CollectionUtils.isEmpty(thirdSystems)) {
            LOGGER.error("can not found any meao system!");
            return Collections.emptyList();
        }
        return thirdSystems;
    }
}
