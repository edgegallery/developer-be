/*
 * Copyright 2022 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.test.util;

import org.edgegallery.developer.util.IpCalculateUtil;
import org.junit.Assert;
import org.junit.Test;

public class IpCalculateUtilTest {

    @Test
    public void testGetNetMask() {
        String maskInt = "24";
        String mask = "255.255.255.0";
        String netMask = IpCalculateUtil.getNetMask(maskInt);
        Assert.assertEquals(mask, netMask);
    }

    @Test
    public void testGetNetMaskReturnNull1() {
        String maskInt = "string";
        Assert.assertNull(IpCalculateUtil.getNetMask(maskInt));
    }

    @Test
    public void testGetNetMaskReturnNull2() {
        String maskInt = "33";
        Assert.assertNull(IpCalculateUtil.getNetMask(maskInt));
    }

    @Test
    public void testGetStartIp() {
        String cidr = "192.168.1.1/24";
        String startIp = IpCalculateUtil.getStartIp(cidr, 5);
        Assert.assertEquals("192.168.1.6", startIp);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testGetStartIpThrowArrayIndexOutOfBoundsException() {
        Assert.assertNull(IpCalculateUtil.getStartIp("192.168.1.1", 0));
    }

}
