/*
 * Copyright 2022 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.test.util;

import org.edgegallery.developer.util.InputParameterUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class InputParameterUtilTest {


	@Test
	public void testGetParams() {
		String inputString = "a=1;b=2";
		Map<String, String> map = InputParameterUtil.getParams(inputString);
		Assert.assertNotNull(map);
		Assert.assertEquals(2, map.size());
		Assert.assertEquals("1", map.get("a"));
	}

	@Test
	public void testGetParamsReturnEmpty1() {
		String inputString = "a=1b=2";
		Map<String, String> map = InputParameterUtil.getParams(inputString);
		Assert.assertEquals(0, map.size());
	}

	@Test
	public void testGetParamsReturnEmpty2() {
		String inputString = "a=1=2;b=2=3";
		Map<String, String> map = InputParameterUtil.getParams(inputString);
		Assert.assertEquals(0, map.size());
	}

	@Test
	public void testGetParamsReturnEmpty3() {
		String inputString = "a12;b23";
		Map<String, String> map = InputParameterUtil.getParams(inputString);
		Assert.assertEquals(0, map.size());
	}

}
