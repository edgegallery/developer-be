/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.test.service.resource.pkgspec;

import java.io.IOException;
import java.util.List;
import org.edgegallery.developer.model.application.vm.Network;
import org.edgegallery.developer.model.resource.pkgspec.PkgSpec;
import org.edgegallery.developer.service.recource.pkgspec.PkgSpecService;
import org.edgegallery.developer.test.DeveloperApplicationTests;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = DeveloperApplicationTests.class)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class PkgSpecServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PkgSpecServiceTest.class);

    private static final String PKG_SPEC_ID = "PKG_SPEC_SUPPORT_DYNAMIC_FLAVOR";
    @Autowired
    private PkgSpecService pkgSpecService;

    @Before
    public void setUp() throws IOException {

    }

    @After
    public void after() {

    }

    @Test
    public void testGetPkgSpecs() {
        List<PkgSpec> pkgSpecs = pkgSpecService.getPkgSpecs();
        Assert.assertNotNull(pkgSpecs);
    }

    @Test
    public void testGetPkgSpecById(){
        PkgSpec pkgSpec = pkgSpecService.getPkgSpecById(PKG_SPEC_ID);
        Assert.assertNotNull(pkgSpec);
    }

    @Test
    public void testGetDefaultPkgSpec(){
        PkgSpec pkgSpec = pkgSpecService.getPkgSpecById("");
        Assert.assertNotNull(pkgSpec);
        Assert.assertEquals(pkgSpec.getId(), PKG_SPEC_ID);
    }
    @Test
    public void testGetPkgSpecNotExist(){
        PkgSpec pkgSpec = pkgSpecService.getPkgSpecById("TestNotExist");
        Assert.assertNull(pkgSpec);
    }

    @Test
    public void testGetNetworkResourceByPkgSpecId(){
        List<Network> networks = pkgSpecService.getNetworkResourceByPkgSpecId(PKG_SPEC_ID);
        Assert.assertNotNull(networks);
    }

    @Test
    public void testGetNetworkResourceByPkgSpecIdNotExist(){
        List<Network> networks = pkgSpecService.getNetworkResourceByPkgSpecId("TestNotExist");
        Assert.assertNull(networks);
    }

    @Test
    public void testGetUserScenes(){
        String userScenes = pkgSpecService.getUseScenes();
        Assert.assertNotNull(userScenes);
    }
}
