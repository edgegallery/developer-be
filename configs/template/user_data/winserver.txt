rem cmd
# Startup script for Win10&WinServer_2012 IPV4 #

for /f "usebackq skip=3 tokens=3*" %%i in ((`netsh interface show interface`)) do netsh interface ip set address "%%j" static $APP_Internet_IP$ $APP_Internet_MASK$ $APP_Internet_GW$
for /f "usebackq skip=4 tokens=3*" %%i in ((`netsh interface show interface`)) do netsh interface ip set address "%%j" static $APP_N6_IP$ $APP_N6_MASK$
for /f "usebackq skip=5 tokens=3*" %%i in ((`netsh interface show interface`)) do netsh interface ip set address "%%j" static $APP_MP1_IP$ $APP_MP1_MASK$

route -p add $UE_IP_SEGMENT$ $APP_N6_GW$